﻿using System;
namespace BeginnenMetCSharp
{
    class HoofdstukTwee
    {
        public static void VariabelenHoofdletters()
        {

            Console.Write("Wat is je naam?: ");
            string name = Console.ReadLine();
            Console.WriteLine("Hallo " + name.ToUpper());

        }

        public static void Optellen()
        {

            Console.WriteLine("Wat is het eerste getal?");
            int eersteGetal = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Wat is het tweede getal?");
            int tweedeGetal = Convert.ToInt32(Console.ReadLine());
            int som = eersteGetal + tweedeGetal;
            Console.WriteLine("De som is " + som + ".");


        }

        public static void VerbruikWagen()
        {
            Console.Write("Geef aantal liter in tank voor de rit: ");

            double tankVoorRit = Double.Parse(Console.ReadLine());

            Console.Write("Geef aantal liter in tank na de rit: ");
            double tankNaRit = Double.Parse(Console.ReadLine());

            Console.Write("Geef kilometerstand1 van je auto voor de rit: ");
            double kilometerStand1 = Double.Parse(Console.ReadLine());

            Console.Write("Geef kilometerstand2 van je auto na de rit: ");
            double kilometerStand2 = Double.Parse(Console.ReadLine());

            double result = 100*(tankVoorRit - tankNaRit) / (kilometerStand2 - kilometerStand1);
            Console.Write("Het verbruik van de wagen is: " + result);

        }

        public static void BeetjeWiskunde()
        {
            int resultaat1 = -1 + 4 * 6;
            int resultaat2 = (35 + 5) % 7;
            int resultaat3 = 14 + (-4 * 6 / 11);
            int resultaat4 = 2 + 15 / 6 * 1 - 7 % 2;

            Console.WriteLine(resultaat1);
            Console.WriteLine(resultaat2);
            Console.WriteLine(resultaat3);
            Console.WriteLine(resultaat4);

        }

        public static void Gemiddelde()
        {
            int gemiddelde = (18 + 11 + 8) / 3;
            Console.WriteLine(gemiddelde);
        }

        public static void Maaltafels()
        {
            int baseNumber = 411;

            Console.WriteLine("1 * " + baseNumber + " is " +(baseNumber * 1));
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("2 * " + baseNumber + " is " + (baseNumber * 2));
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("3 * " + baseNumber + " is " + (baseNumber * 3));
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("4 * " + baseNumber + " is " + (baseNumber * 4));
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("5 * " + baseNumber + " is " + (baseNumber * 5));
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("6 * " + baseNumber + " is " + (baseNumber * 6));
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("7 * " + baseNumber + " is " + (baseNumber * 7));
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("8 * " + baseNumber + " is " + (baseNumber * 8));
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("9 * " + baseNumber + " is " + (baseNumber * 9));
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("10 * " + baseNumber + " is " + (baseNumber * 10));
            Console.ReadKey();
        }

        public static void Ruimte()
        {
            double massa = 69;

            Console.WriteLine("Op Mercurius voel je je alsof je " + (massa * 0.38) + " kg weegt.");
            Console.WriteLine("Op Venus voel je je alsof je " + (massa * 0.91) + " kg weegt.");
            Console.WriteLine("Op Aarde voel je je alsof je " + (massa*1.00) + " kg weegt.");
            Console.WriteLine("Op Jupiter voel je je alsof je " + (massa * 2.34) +" kg weegt.");
            Console.WriteLine("Op Saturnus voel je je alsof je " + (massa * 1.06) + " kg weegt.");
            Console.WriteLine("Op Uranus voel je je alsof je " + (massa * 0.92) + " kg weegt.");
            Console.WriteLine("Op Neptunus voel je je alsof " + (massa * 1.19) + " kg weegt.");
            Console.WriteLine("Op Pluto voel je je alsof " + (massa * 0.06) + " kg weegt.");
            Console.ReadKey();

        }

        public static void Keuzemenu()
        {

        }

    }
}
