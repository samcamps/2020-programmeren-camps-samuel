﻿using System;
namespace BeginnenMetCSharp
{
    public class APDotCom
    {
        public static void Bestel()
        {

            Console.WriteLine("Prijs van een boek?");
            double priceBook = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een CD");
            double priceCD = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een servies");
            double priceServies = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een springkasteel?");
            double priceSpringkasteel = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Aantal boeken?");
            double numberBook = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal CD's");
            double numberCD = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal serviezen");
            double numberServies = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal springkastelen");
            double numberSpringkasteel = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Percentage korting?");
            double discount = Convert.ToDouble(Console.ReadLine());

            double total = (numberBook * priceBook) + (numberCD * priceCD) + (numberServies * priceServies) + (numberSpringkasteel * priceSpringkasteel);

            Console.WriteLine("Uw kasticket\n-------------");
            Console.WriteLine($"boek x {numberBook}: {numberBook * priceBook:F2}");
            Console.WriteLine($"CD x {numberCD}: {numberCD * priceCD:F2}");
            Console.WriteLine($"servies x {numberServies}: {numberServies * priceServies:F2}");
            Console.WriteLine($"springkasteel x {numberSpringkasteel}: {numberSpringkasteel * priceSpringkasteel:F2}");
            Console.WriteLine($"KORTING: {discount}%");
            Console.WriteLine($"TOTAAL VOOR KORTING: {total:F2}");
            Console.WriteLine($"TOTAAL: {total * (1 - discount / 100):F2}");

        }

        public static void BestelMetVraagEnAanbod()
        {

            Console.Clear();

            Random mygen = new Random(); 

            Console.WriteLine("Prijs van een boek?");
            double priceBook = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een CD?");
            double priceCD = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een servies?");
            double priceServies = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een springkasteel?");
            double priceSpringkasteel = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Aantal boeken?");
            double numberBook = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal CD's?");
            double numberCD = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal serviezen?");
            double numberServies = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal springkastelen?");
            double numberSpringkasteel = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Percentage korting?");
            double discount = Convert.ToDouble(Console.ReadLine());

            int randomBoek = mygen.Next(-50, 50);
            int randomCD = mygen.Next(-50, 50);
            int randomServies = mygen.Next(-50, 50);
            int randomSpringkasteel = mygen.Next(-50, 50);
            double total = (numberBook * (priceBook + priceBook * (double)randomBoek / 100))
                + (numberCD * (priceCD + priceCD * (double)randomCD/100 )
                + (numberServies * (priceServies + priceServies * (double)randomServies / 100))
                + (numberSpringkasteel * (priceSpringkasteel + priceSpringkasteel * (double) randomSpringkasteel / 100)));

            Console.WriteLine("Uw kasticket\n-------------");
            Console.WriteLine($"vraag en aanbod boeken: {randomBoek}%");
            Console.WriteLine($"vraag en aanbod CD's: {randomCD}%");
            Console.WriteLine($"vraag en aanbod serviezen: {randomServies}%");
            Console.WriteLine($"vraag en aanbod springkastelen: {randomSpringkasteel}%");
                        
            Console.WriteLine($"boek x {numberBook}: {numberBook * (priceBook +priceBook *(double)randomBoek / 100):F2}");
            Console.WriteLine($"CD x {numberCD}: {numberCD * (priceCD + priceCD * (double)randomCD / 100):F2}");
            Console.WriteLine($"servies x {numberServies}: {numberServies * (priceServies + priceServies * (double)randomServies / 100):F2}");
            Console.WriteLine($"springkasteel x {numberSpringkasteel}: {numberSpringkasteel * (priceSpringkasteel +priceSpringkasteel * (double)randomSpringkasteel / 100):F2}");
            Console.WriteLine($"KORTING: {discount}%");
            Console.WriteLine($"TOTAAL VOOR KORTING: {total:F2}");
            Console.WriteLine($"TOTAAL: {total * (1 - discount / 100):F2}");

        }

        public static void BestelConditioneel()
        {

            Console.Clear();

            Random mygen = new Random();

            Console.WriteLine("Prijs van een boek?");
            double priceBook = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een CD?");
            double priceCD = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een servies?");
            double priceServies = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Prijs van een springkasteel?");
            double priceSpringkasteel = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Aantal boeken?");
            double numberBook = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal CD's?");
            double numberCD = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal serviezen?");
            double numberServies = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Aantal springkastelen?");
            double numberSpringkasteel = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Percentage korting?");
            double discount = Convert.ToDouble(Console.ReadLine());
            if(discount > 30)
            {
                Console.WriteLine("Waarschuwing: het ingevoerde percentage is hoog!");
            }

            Console.WriteLine("Worden prijsstijgingen en -dalingen toegepast? (J/N)");
            string prijsStijging = Console.ReadLine();

            if (prijsStijging == "J")
            {

                int randomBoek = mygen.Next(-50, 50);
                int randomCD = mygen.Next(-50, 50);
                int randomServies = mygen.Next(-50, 50);
                int randomSpringkasteel = mygen.Next(-50, 50);


                double total = (numberBook * (priceBook + priceBook * (double)randomBoek / 100))
                    + (numberCD * (priceCD + priceCD * (double)randomCD / 100)
                    + (numberServies * (priceServies + priceServies * (double)randomServies / 100))
                    + (numberSpringkasteel * (priceSpringkasteel + priceSpringkasteel * (double)randomSpringkasteel / 100)));

                Console.WriteLine("Uw kasticket\n-------------");

                if (randomBoek > 0)
                {
                    Console.Write($"vraag en aanbod boeken: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{randomBoek}%");
                    Console.ResetColor();

                }
                else
                {
                    Console.Write($"vraag en aanbod boeken: ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{randomBoek}%");
                    Console.ResetColor();
                }

                if (randomCD > 0)
                {
                    Console.Write($"vraag en aanbod CD's: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{randomCD}%");
                    Console.ResetColor();

                }
                else
                {
                    Console.Write($"vraag en aanbod CD's: ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{randomCD}%");
                    Console.ResetColor();
                }

                if (randomServies > 0)
                {
                    Console.Write($"vraag en aanbod serviezen: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{randomServies}%");
                    Console.ResetColor();

                }
                else
                {
                    Console.Write($"vraag en aanbod serviezen: ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{randomServies}%");
                    Console.ResetColor();
                }

                if (randomSpringkasteel > 0)
                {
                    Console.Write($"vraag en aanbod springkastelen: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{randomSpringkasteel}%");
                    Console.ResetColor();

                }
                else
                {
                    Console.Write($"vraag en aanbod springkastelen: ");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"{randomSpringkasteel}%");
                    Console.ResetColor();
                }

                Console.WriteLine($"boek x {numberBook}: {numberBook * (priceBook + priceBook * (double)randomBoek / 100):F2}");
                Console.WriteLine($"CD x {numberCD}: {numberCD * (priceCD + priceCD * (double)randomCD / 100):F2}");
                Console.WriteLine($"servies x {numberServies}: {numberServies * (priceServies + priceServies * (double)randomServies / 100):F2}");
                Console.WriteLine($"springkasteel x {numberSpringkasteel}: {numberSpringkasteel * (priceSpringkasteel + priceSpringkasteel * (double)randomSpringkasteel / 100):F2}");
                Console.WriteLine($"KORTING: {discount}%");
                Console.WriteLine($"TOTAAL VOOR KORTING: {total:F2}");
                Console.WriteLine($"TOTAAL: {total * (1 - discount / 100):F2}");
            }
            else
            {
                double total = (numberBook * priceBook) + (numberCD * priceCD) + (numberServies * priceServies) + (numberSpringkasteel * priceSpringkasteel);

                Console.WriteLine("Uw kasticket\n-------------");
                Console.WriteLine($"boek x {numberBook}: {numberBook * priceBook:F2}");
                Console.WriteLine($"CD x {numberCD}: {numberCD * priceCD:F2}");
                Console.WriteLine($"servies x {numberServies}: {numberServies * priceServies:F2}");
                Console.WriteLine($"springkasteel x {numberSpringkasteel}: {numberSpringkasteel * priceSpringkasteel:F2}");
                Console.WriteLine($"KORTING: {discount}%");
                Console.WriteLine($"TOTAAL VOOR KORTING: {total:F2}");
                Console.WriteLine($"TOTAAL: {total * (1 - discount / 100):F2}");
            }

        }

        public static void BestelMeerdere()
        {

            Console.Clear();

            Random mygen = new Random();
            string answer;

            do
            {
                Console.WriteLine("Prijs van een boek?");
                double priceBook = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Prijs van een CD?");
                double priceCD = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Prijs van een servies?");
                double priceServies = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Prijs van een springkasteel?");
                double priceSpringkasteel = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Aantal boeken?");
                double numberBook = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Aantal CD's?");
                double numberCD = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Aantal serviezen?");
                double numberServies = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Aantal springkastelen?");
                double numberSpringkasteel = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Percentage korting?");
                double discount = Convert.ToDouble(Console.ReadLine());
                if (discount > 30)
                {
                    Console.WriteLine("Waarschuwing: het ingevoerde percentage is hoog!");
                }

                Console.WriteLine("Worden prijsstijgingen en -dalingen toegepast? (J/N)");
                string prijsStijging = Console.ReadLine();

                if (prijsStijging == "J")
                {

                    int randomBoek = mygen.Next(-50, 50);
                    int randomCD = mygen.Next(-50, 50);
                    int randomServies = mygen.Next(-50, 50);
                    int randomSpringkasteel = mygen.Next(-50, 50);

                    double total = (numberBook * (priceBook + priceBook * (double)randomBoek / 100))
                        + (numberCD * (priceCD + priceCD * (double)randomCD / 100)
                        + (numberServies * (priceServies + priceServies * (double)randomServies / 100))
                        + (numberSpringkasteel * (priceSpringkasteel + priceSpringkasteel * (double)randomSpringkasteel / 100)));

                    Console.WriteLine("Uw kasticket\n-------------");

                    if (randomBoek > 0)
                    {
                        Console.Write($"vraag en aanbod boeken: ");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"{randomBoek}%");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.Write($"vraag en aanbod boeken: ");
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"{randomBoek}%");
                        Console.ResetColor();
                    }

                    if (randomCD > 0)
                    {
                        Console.Write($"vraag en aanbod CD's: ");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"{randomCD}%");
                        Console.ResetColor();

                    }
                    else
                    {
                        Console.Write($"vraag en aanbod CD's: ");
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"{randomCD}%");
                        Console.ResetColor();
                    }

                    if (randomServies > 0)
                    {
                        Console.Write($"vraag en aanbod serviezen: ");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"{randomServies}%");
                        Console.ResetColor();

                    }
                    else
                    {
                        Console.Write($"vraag en aanbod serviezen: ");
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"{randomServies}%");
                        Console.ResetColor();
                    }

                    if (randomSpringkasteel > 0)
                    {
                        Console.Write($"vraag en aanbod springkastelen: ");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"{randomSpringkasteel}%");
                        Console.ResetColor();

                    }
                    else
                    {
                        Console.Write($"vraag en aanbod springkastelen: ");
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"{randomSpringkasteel}%");
                        Console.ResetColor();
                    }

                    Console.WriteLine($"boek x {numberBook}: {numberBook * (priceBook + priceBook * (double)randomBoek / 100):F2}");
                    Console.WriteLine($"CD x {numberCD}: {numberCD * (priceCD + priceCD * (double)randomCD / 100):F2}");
                    Console.WriteLine($"servies x {numberServies}: {numberServies * (priceServies + priceServies * (double)randomServies / 100):F2}");
                    Console.WriteLine($"springkasteel x {numberSpringkasteel}: {numberSpringkasteel * (priceSpringkasteel + priceSpringkasteel * (double)randomSpringkasteel / 100):F2}");
                    Console.WriteLine($"KORTING: {discount}%");
                    Console.WriteLine($"TOTAAL VOOR KORTING: {total:F2}");
                    Console.WriteLine($"TOTAAL: {total * (1 - discount / 100):F2}");
                }
                else
                {
                    double total = (numberBook * priceBook) + (numberCD * priceCD) + (numberServies * priceServies) + (numberSpringkasteel * priceSpringkasteel);

                    Console.WriteLine("Uw kasticket\n-------------");
                    Console.WriteLine($"boek x {numberBook}: {numberBook * priceBook:F2}");
                    Console.WriteLine($"CD x {numberCD}: {numberCD * priceCD:F2}");
                    Console.WriteLine($"servies x {numberServies}: {numberServies * priceServies:F2}");
                    Console.WriteLine($"springkasteel x {numberSpringkasteel}: {numberSpringkasteel * priceSpringkasteel:F2}");
                    Console.WriteLine($"KORTING: {discount}%");
                    Console.WriteLine($"TOTAAL VOOR KORTING: {total:F2}");
                    Console.WriteLine($"TOTAAL: {total * (1 - discount / 100):F2}");
                }

                Console.WriteLine("\nWil je nog een bestelling plaatsen? J/N");
                answer = Console.ReadLine();
                Console.Write("\n");

            } while (answer == "J");
        }

    }
}
