﻿using System;
namespace BeginnenMetCSharp
{
    public class HoofdstukDrie
    {
        public static void StringInterpolation()
        {

            // Maaltafels
            

            int baseNumber = 411;

            Console.WriteLine($"1 * {baseNumber} is {baseNumber * 1}.");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"2 * {baseNumber} is {baseNumber * 2}.");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"3 * {baseNumber} is {baseNumber * 3}.");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"4 * {baseNumber} is {baseNumber * 4}.");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"5 * {baseNumber} is {baseNumber * 5}.");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"6 * {baseNumber} is {baseNumber * 6}.");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"7 * {baseNumber} is {baseNumber * 7}.");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"8 * {baseNumber} is {baseNumber * 8}.");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"9 * {baseNumber} is {baseNumber * 9}.");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"10 * {baseNumber} is {baseNumber * 10}.");
            Console.ReadKey();
            

            // Ruimte


            double weight = 100;

            Console.WriteLine($"Op Mercurius voel je je alsof je {weight * 0.38}kg weegt.");
            Console.WriteLine($"Op Venus voel je je alsof je {weight * 0.91}kg weegt.");
            Console.WriteLine($"Op Aarde voel je je alsof je {weight * 1}kg weegt.");
            Console.WriteLine($"Op Mars voel je je alsof je {weight * 0.38}kg weegt.");
            Console.WriteLine($"Op Jupiter voel je je alsof je {weight * 2.34}kg weegt.");
            Console.WriteLine($"Op Saturnus voel je je alsof je {weight * 1.06}kg weegt.");
            Console.WriteLine($"Op Uranus voel je je alsof je {weight * 0.92}kg weegt.");
            Console.WriteLine($"Op Neptunus voel je je alsof je {weight * 1.19}kg weegt.");
            Console.WriteLine($"Op Pluto voel je je alsof je {weight * 0.06}kg weegt.");
            Console.ReadKey();

        }


        public static void BerekenBTW()
        {

            double amount;
            double percentage;
            

            Console.Write("Geef het bedrag: ");
            amount = Convert.ToInt32(Console.ReadLine());

            Console.Write("Geef het btw-percentage: ");
            percentage = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine($"Het bedrag {amount} met {percentage}% bedraagt {amount + ((amount * percentage) / 100)}");

        }


        public static void LeetSpeak()
        {

            string zin;
            string leetzin;

            Console.Write("Geef je tekst in: ");
            zin = Console.ReadLine();
            leetzin = zin.Replace('a','@');
            leetzin = leetzin.Replace(" ","");
            Console.WriteLine(leetzin);
        }

        public static void Instructies()
        {

            Console.Write("Wat is je naam?: ");
            string name = Console.ReadLine();
            Console.Write("Wat is de naam van je cursus?: ");
            string course = Console.ReadLine();
            Console.Write($@"Maak een map als volgt: \{name.Substring(0, 3).ToUpper()}\{course}");

        }

        public static void Lotto()
        {
            Console.Write("Wat zijn je cijfers? (tussen 0 en 45)?: ");
            string cijfers = Console.ReadLine();
            Console.Write($"Je cijfers zijn: \n{cijfers.Substring(0, 2)}\t{cijfers.Substring(3, 2)}\t{cijfers.Substring(6, 2)}\n" +
                $"{cijfers.Substring(9, 2)}\t{cijfers.Substring(12,2)}\t{cijfers.Substring(15,2)}");
        }

        public static void SuperLotto()
        {
            /*Dit is een variatie op de vorige oefening. 
             * Schrijf nu een methode SuperLotto. 
             * Het enige verschil is dat de gebruiker getallen ook met één cijfer mag noteren.
             */

            Console.Write("Wat zijn je cijfers? (tussen 0 en 45)?: ");
            string cijfers = Console.ReadLine();
            int lengte1 = cijfers.IndexOf(",");
            int eersteCijfer = Convert.ToInt32(cijfers.Substring(0, lengte1));


            int lengte2 =  cijfers.Substring(lengte1 + 1).IndexOf(",");
            int tweedeCijfer = Convert.ToInt32(cijfers.Substring(lengte1 + 1, lengte2));


            int lengte3 = cijfers.Substring(lengte1+ lengte2 + 2).IndexOf(",");
            int derdeCijfer = Convert.ToInt32(cijfers.Substring(lengte1 + lengte2 + 2, lengte3));



            int lengte4 = cijfers.Substring(lengte1 +lengte2+lengte3+3).IndexOf(",");
            int vierdeCijfer = Convert.ToInt32(cijfers.Substring(lengte1 + lengte2 + lengte3 + 3, lengte4));

            int lengte5 = cijfers.Substring(lengte1 + lengte2 + lengte3 + lengte4 + 4).IndexOf(",");
            int vijfdeCijfer = Convert.ToInt32(cijfers.Substring(lengte1 + lengte2 + lengte3 + lengte4 + 4, lengte5));


            int zesdeCijfer = Convert.ToInt32((cijfers.Substring(lengte1 + lengte2 + lengte3 + lengte4 + lengte5 + 5)));

            /*  10,20,30,40,50,60

                        string cijfer1result = cijfers.Substring(0, cijfers.IndexOf(","));
                        string cijfer2 = cijfers.Substring((cijfer1result.Length)+1);
                        string cijfer2result = cijfer2.Substring(0, cijfer2.IndexOf(","));
                        string cijfer3 = cijfer2.Substring((cijfer2result.Length) + 1);
                        string cijfer3result = cijfer3.Substring(0, cijfer3.IndexOf(","));
                        string cijfer4 = cijfer3.Substring((cijfer3result.Length) + 1);
                        string cijfer4result = cijfer4.Substring(0, cijfer4.IndexOf(","));
                        string cijfer5 = cijfer4.Substring((cijfer4result.Length) + 1);
                        string cijfer5result = cijfer5.Substring(0, cijfer5.IndexOf(","));
                        string cijfer6result= cijfer5.Substring((cijfer5result.Length) + 1);*/

            Console.Write($"{eersteCijfer:D2}\t{tweedeCijfer:D2}\t{derdeCijfer:D2}\n{vierdeCijfer:D2}\t{vijfdeCijfer:D2}\t{zesdeCijfer:D2}");
            

        }


    }
}
