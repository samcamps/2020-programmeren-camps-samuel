﻿using System;

namespace BeginnenMetCSharp
{
    class Program
    {
        static void Main(string[] args)
        {


            // EenProgrammaSchrijvenInCSharp.ZegGoeienDag();

             EenProgrammaSchrijvenInCSharp.GekleurdeRommelZin();

            // HoofdstukTwee.VariabelenHoofdletter();

            //HoofdstukTwee.Optellen();
            // HoofdstukTwee.VerbruikWagen();

            //HoofdstukTwee.Ruimte();

            //HoofdstukVier.BMIBerekenaar();
            //HoofdstukVier.Orakeltje();

            //HoofdstukVijf.SimpeleRekenMachine();

            // Hoofdmenu();

            //kleurcodes niet maken en ruimtespecifiek ook niet

            //APDotCom.BestelMeerdere();
            //HoofdstukZeven.DemonstreerKleintjes();
            //HoofdstukAcht.Galgje();
            
        }


        public static void Hoofdmenu()
        {
            Console.WriteLine("Kies uit volgende klassen door een cijfer in te tikken:");
            Console.WriteLine("3 - HoofdstukDrie\n4 - HoofdstukVier\n5 - HoofstukVijf");
            int keuze = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            switch (keuze)
            {
 
                case 4:
                    HoofdstukVier.KeuzeMenu();
                    break;
                case 5:
                    HoofdstukVijf.KeuzeMenu();
                    break;
            }


        }
        
    }

    
}
