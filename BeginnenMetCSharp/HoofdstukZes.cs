﻿using System;
namespace BeginnenMetCSharp
{
    public class HoofdstukZes
    {
        public static void BasisEen()
        {
            //1.	Toon alle natuurlijke getallen van 1 tot 100 onder mekaar op het scherm
            //, gebruikmakende van een while.

            int i = 1;


            while (i < 100)
            {
                Console.WriteLine($"{i}");
                i = i + 1;

            }
        }

        public static void BasisTwee()
        {
            //2.Toon alle gehele getallen van -100 tot 100 onder mekaar op het scherm, gebruikmakende van een while.

            int i = -100;


            while (i < 100)
            {
                Console.WriteLine($"{i}");
                i = i + 1;

            }

        }


        public static void BasisDrie()
        {

            // 3.Toon alle natuurlijke getallen van 1 tot 100 onder mekaar op het scherm, gebruikmakende van een for. 

            for (int i = 1; i < 100; i++)
            {
                Console.WriteLine(i);
            }


        }

        public static void BasisVier()
        {
            //4.Toon alle gehele getallen van -100 tot 100 onder mekaar op het scherm, gebruikmakende van een for.

            for (int i = -100; i < 100; i++)
            {
                if (i % 2 == 00)
                {
                    Console.WriteLine(i);
                }
            }
        }

        public static void BasisVijf()
        {
            //5.Toon alle gehele getallen tussen 1 en 100 onder mekaar op het scherm die een veelvoud van 6 en 8 zijn. Gebruik een while.

            int i = 1;

            while (i < 100)
            {
                if (i % 6 == 0 && i % 8 == 0)
                {
                    Console.WriteLine(i);
                }
                i++;
            }
        }



        public static void BasisZes()
        {
            //Toon alle getallen tussen 1 en 100 onder mekaar op het scherm die een veelvoud van 6 en 8 zijn.Gebruik een for.

            for (int i = 1; i < 100; i++)
            {
                if (i % 6 == 0 && i % 8 == 0)
                {
                    Console.WriteLine(i);
                }
            }
        }


        public static void Maaltafels()
        {
            int baseNumber = 411;

            for (int i = 1; i < 11; i++)
            {
                Console.WriteLine($"{i} * {baseNumber} is {baseNumber * i}");
                Console.ReadKey();
                Console.Clear();
            }



        }

        public static void RNA()
        {
            string answer;
            string result = "";
            Console.WriteLine("Geef nucleotiden:");

            do
            {

                answer = Console.ReadLine();

                switch (answer)
                {

                    case "G":
                        result = result + "C";
                        break;

                    case "C":
                        result = result + "G";
                        break;

                    case "T":
                        result = result + "A";
                        break;

                    case "A":
                        result = result + "U";
                        break;

                }
            }
            while (answer != "");
            Console.WriteLine($"Je resultaat is {result}");
        }

        public static void OpwarmerPlusEen()
        {
            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i < n; i++)

            {
                Console.WriteLine(i);
            }
        }

        public static void OpwarmerPlusTwee()
        {
            Console.WriteLine("Geef een getal:");


            for (int n = Convert.ToInt32(Console.ReadLine()); n > 0; n--)

            {
                Console.WriteLine(n);
            }
        }

        public static void OpwarmerPlusDrie()
        {

            for (int n = 1; n < 100; n++)

            {
                if (n % 2 == 0)
                {
                    Console.WriteLine(n);
                }
            }
        }

        public static void OpwarmerPlusVier()
        {

            for (int n = 1; n < 100; n++)

            {
                if (n % 2 == 1)
                {
                    Console.WriteLine(n);
                }
            }
        }

        public static void OpwarmerPlusVijf()
        {

            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());
            int i = 0;
            int result = 0;

            while (i <= n)
            {
                result = result + i;
                i++;

            }
            Console.WriteLine(result);

        }

        public static void OpwarmerPlusZes()
        {
            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());
            int i = 0;
            int result = 0;

            while (i <= n)
            {

                if (i % 2 == 0)
                {
                    result = result + i;
                }

                i++;

            }
            Console.WriteLine(result);

        }

        public static void OpwarmerPlusZeven()
        {
            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());
            int i = 0;
            int result = 0;

            while (i <= n)
            {

                if (i % 2 == 1)
                {
                    result = result + i;
                }

                i++;

            }
            Console.WriteLine(result);


        }

        public static void OpwarmerPlusAcht()
        {
            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());
            int aantal = 0;

            while (n > 0)
            {
                n = n / 10;
                aantal++;


            }

            Console.WriteLine($"Aantal digits is {aantal}");


        }

        //Schrijf een programma dat alle ascii karakters en hun waarde toont
        //van 10 tot n (tip: char c = Convert.ToChar(65); zal hoofdletter A tonen)

        public static void OpwarmerPlusTien()
        {
            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());
            int i = 10;

            while (i < n)
            {

                Console.WriteLine(Convert.ToChar(i));
                i++;


            }

        }

        public static void OpwarmerPlusElf()
        {

            char karakter = 'a';
            while (karakter <= 'z')
            {
                Console.WriteLine(karakter);
                karakter++;
            }


        }


        public static void OpwarmerPlusTwaalf()
        {
            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Tot welke macht verheffen?:");
            int m = Convert.ToInt32(Console.ReadLine());
            int result = n;

            for (int i = 1; i < m; i++)
            {
                result = result * n;
            }
            if (m == 0)
            {
                Console.WriteLine(1);
            }
            else
            {
                Console.WriteLine(result);
            }

        }

        public static void OpwarmerPlusDertien()
        {
            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    Console.WriteLine(i);
                }

            }

        }

        public static void OpwarmerPlusVeertien()
        {
            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());
            int getal1 = 0;
            int getal2 = 1;
            int result;

            if (n == 1)
            {
                Console.WriteLine(getal1);
            }

            else if (n == 2)
            {
                Console.WriteLine(getal1);
                Console.WriteLine(getal2);
            }

            else
            {
                Console.WriteLine(getal1);
                Console.WriteLine(getal2);

                for (int i = 2; i < n; i++)
                {

                    result = getal1 + getal2;

                    Console.WriteLine(result);

                    getal1 = getal2;
                    getal2 = result;
                }
            }



        }

        public static void PriemChecker()
        {
            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());

            bool isPrime = true;



            for (int i = 1; i < n && isPrime; i++)
            {
                if (n % i == 0)
                {
                    isPrime = false;

                }
                Console.WriteLine(i);

            }


            if (isPrime && n != 1)
            {
                Console.WriteLine($"{n} is een priemgetal");
            }
            else
            {

                Console.WriteLine($"{n} is Geen priemgetal");
            }



        }

        public static void PriemGenerator()
        {
            Console.WriteLine("Geef een getal:");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Alle priemgetallen kleiner of gelijk aan {n} zijn:");

            for (int i = 2; i <= n; i++)
            {

                bool isPrime = true;

                for (int j = 2; j < i; j++)
                {
                    if (i % j == 0)
                    {
                        isPrime = false;

                    }


                }

                if (isPrime == true)
                {
                    Console.WriteLine(i);
                }


            }


        }


        public static void Boekhouder()
        {
            int balans = 0;
            int sumPos = 0;
            int sumNeg = 0;
            double i = 0;
            double avg = 0;
            int number;

            while (true)
            {
                Console.Write("Geef een getal: ");
                number = Convert.ToInt32(Console.ReadLine());
                balans = balans + number;
                if (number > 0)
                {
                    sumPos = sumPos + number;
                }
                else
                {
                    sumNeg = sumNeg + number;
                }

                i++;
                avg = balans / i;

                Console.WriteLine($"De balans is: {balans}");
                Console.WriteLine($"Som positieve getalen: {sumPos}");
                Console.WriteLine($"Som negatieve getalen: {sumNeg}");
                Console.WriteLine($"Het gemiddelde is: {avg:F2}");
                Console.WriteLine("\n");
            }

        }
        enum Opties
        {
            Papier, Steen, Schaar
        }

        public static void PapierSteenSchaar()
        {

            int keuzePlayer;
            int keuzePC;
            Random mygen = new Random();
            int scorePlayer = 0;
            int scorePC = 0;

            while (scorePC != 10 && scorePlayer != 10)
            {


                Console.WriteLine("Maak een keuze:");
                Console.WriteLine($"{(int)Opties.Papier} {Opties.Papier}");
                Console.WriteLine($"{(int)Opties.Steen} {Opties.Steen}");
                Console.WriteLine($"{(int)Opties.Schaar} {Opties.Schaar}");

                keuzePlayer = Convert.ToInt32((Console.ReadLine()));

                keuzePC = mygen.Next(0, 3);

                Console.WriteLine($"De computer kiest {(Opties)keuzePC}");

                if (keuzePlayer == 0 && keuzePC == 1)
                {
                    scorePlayer++;
                    Console.WriteLine($"Jij wint deze ronde!\nJij hebt {scorePlayer} punten, de computer heeft {scorePC} punten");
                }
                else if (keuzePlayer == 1 && keuzePC == 2)
                {
                    scorePlayer++;
                    Console.WriteLine($"Jij wint deze ronde!\nJij hebt {scorePlayer} punten, de computer heeft {scorePC} punten");
                }
                else if (keuzePlayer == 2 && keuzePC == 0)
                {
                    scorePlayer++;
                    Console.WriteLine($"Jij wint deze ronde!\nJij hebt {scorePlayer} punten, de computer heeft {scorePC} punten");

                }
                else if (keuzePlayer == keuzePC)
                {
                    Console.WriteLine($"Niemand win deze ronde!\nJij hebt {scorePlayer} punten, de computer heeft {scorePC} punten");
                }

                else
                {
                    scorePC++;
                    Console.WriteLine($"De computer wint deze ronde!\nJij hebt {scorePlayer} punten, de computer heeft {scorePC} punten");

                }
            }

            if (scorePlayer == 10)
            {
                Console.WriteLine("Jij hebt gewonnen!");
            }
            else
            {
                Console.WriteLine("De computer heeft gewonnen");
            }



        }

        public static void Rugby()
        {

            for (int i = 1; i <= 16; i++)
            {
                if (i == 13)
                {
                    continue;
                }
                Console.WriteLine("Geef de naam in van de speler:");
                string name = Console.ReadLine();
                Console.WriteLine($"{name} heeft nummer: {i}.");
            }
        }

        public static void VanNulTotNegenennegentig()
        {
            for (int j = 0; j < 10; j++)
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.Write($"{j}{i}\t");

                }
                Console.WriteLine("");
            }

        }

        public static void RechteSterrenDriehoek()
        {
            Console.WriteLine("Hoeveel rijen wil je zien?");
            int number = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= number; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("*");
                }
                Console.Write("\n");
            }


        }

        public static void LangeSterrenDriehoek()
        {
            Console.WriteLine("Hoeveel rijen wil je zien?");
            int number = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= number; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("*");
                }
                Console.Write("\n");
            }

            for (int i = number - 1; i > 0; i--)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("*");
                }
                Console.Write("\n");
            }


        }

        public static void Som()
        {
            Random myrandom = new Random();
            int sum;
            int a;
            string nogeens;

            do
            {
                sum = 0;

                for (int i = 0; i < 5; i++)
                {
                    a = myrandom.Next(1, 10);
                    sum = sum + a;
                    Console.Write($"{a}\t");

                }
                Console.WriteLine("\nGeef de som: ");
                int answer;
                answer = Convert.ToInt32(Console.ReadLine());

                if (answer == sum)
                {
                    Console.WriteLine("Juiste som!");
                }

                else
                {
                    Console.WriteLine($"Fout! De som is {sum}.");
                }

                Console.WriteLine("Nog een keer? (stoppen met \"stop\")");
                nogeens = Console.ReadLine();

            } while (nogeens != "stop");

        }

        public static void MaaltafelTest()
        {

            int tafel;
            int answer;
            int b;
            int totalright = 0;
            int totalwrong = 0;

            Random mygen = new Random();

            while (true)
            {
                Console.WriteLine("Welke maaltafel wil je oefenen? (0 om te stoppen): ");
                tafel = Convert.ToInt32(Console.ReadLine());
                if (tafel == 0)
                {
                    break;
                }

                for (int i = 0; i < 5; i++)

                {
                    b = mygen.Next(1, 10);
                    Console.WriteLine($"Hoeveel is {tafel} * {b} (deze tafel stoppen: 0)");
                    answer = Convert.ToInt32(Console.ReadLine());
                    if (answer == (b * tafel))
                    {
                        Console.WriteLine("Dat is correct!");
                        totalright++;
                    }

                    else if (answer == 0)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Dat is niet correct!");
                        totalwrong++;
                    }

                }
            }
            Console.WriteLine($"Totaal juiste antwoorden: {totalright}");
            Console.WriteLine($"Totaal foute antwoorden: {totalwrong}");


        }
    }
}