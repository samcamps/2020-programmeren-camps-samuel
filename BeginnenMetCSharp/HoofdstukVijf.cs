﻿using System;
namespace BeginnenMetCSharp
{
    public class HoofdstukVijf
    {
        public static void BMIBerekenaar()
        {
            Console.WriteLine("Hoeveel weeg je in kg?");
            double weight = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double length = Convert.ToDouble(Console.ReadLine());
            double result = weight / Math.Pow(length, 2);
            Console.WriteLine($"Je BMI bedraagt {result:F2}.");

            if (result < 18.5)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ondergewicht");
            }

            else if (result < 25)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Normaal gewicht");
            }

            else if (result < 30)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Overgewicht");
            }

            else if (result < 40)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Zwaarlijvig");

            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("Obesitas");

            }

        }



        public static void Schoenverkoper()
        {
            const double price = 20.0;
            const double reducedPrice = 10;

            Console.WriteLine("Vanaf hoeveel schoenen reductie?");
            int numberStartReduction = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("Hoeveel paar schoenen wil je kopen?");
            int pairs = Convert.ToInt32(Console.ReadLine());

            if (pairs > numberStartReduction)
            {
                Console.WriteLine($"Je moet {pairs * reducedPrice} euro betalen.");
            }
            else
            {
                Console.WriteLine($"Je moet {pairs * price} euro betalen.");
            }

        }

        public static void SimpeleRekenMachine()
        {
            Console.WriteLine("Geef een eerste getal in");
            int firstNumber = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("Geef een tweede getal in");
            int secondNumber = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("Welke berekening wil je maken (+,-,*,/)");
            string bewerking = Console.ReadLine();

            double result;

            if (bewerking == "+")
            {
                result = firstNumber + secondNumber;

            }

            else if (bewerking == "-")
            {
                result = firstNumber - secondNumber;
            }

            else if (bewerking == "*")
            {
                result = firstNumber * secondNumber;
            }
            else
            {
                result = (double)firstNumber / (double)secondNumber;
            }


            Console.WriteLine($"De uitkomst is {result}");

        }


        public static void ohmBerekenaar()
        {
            Console.WriteLine("Wat wil je berekenen? spanning, weerstand of stroomsterkte?");
            string answer = Console.ReadLine();

            if (answer == "stroomsterkte")
            {
                Console.WriteLine("Wat is de spanning?");
                double voltage = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Wat is de weerstand?");
                double resistance = Convert.ToDouble(Console.ReadLine());
                double result = voltage / resistance;
                Console.WriteLine($"De stroomsterkte bedraagt {result}");
            }

            else if (answer == "spanning")
            {
                Console.WriteLine("Wat is de weerstand?");
                double resistance = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Wat is de stroomsterkte?");
                double current = Convert.ToDouble(Console.ReadLine());
                double result = resistance * current;
                Console.WriteLine($"De spaning bedraagt {result}");
            }

            else if (answer == "weerstand")
            {
                Console.WriteLine("Wat is de spanning?");
                double voltage = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine("Wat is de stroomsterkte?");
                double current = Convert.ToDouble(Console.ReadLine());
                double result = voltage / current;
                Console.WriteLine($"De stroomsterkte bedraagt {result}");
            }
        }

        public static void Schrikkeljaar()
        {

            //De gebruiker voert een jaartal in en jouw programma toont of het wel of geen schrikkeljaar is.
            //Een schrikkeljaar is deelbaar door 4, behalve als het ook deelbaar is door 100, tenzij het wél deelbaar is door 400.

            Console.WriteLine("Geef een jaartal in: ");
            int jaartal = Convert.ToInt32(Console.ReadLine());

            if (jaartal % 4 == 0)
            {
                if (jaartal % 100 == 0 )
                {
                    if (jaartal % 400 == 0)
                    {
                        Console.WriteLine($"{jaartal} is toch wel een schrikkeljaar");
                    }
                    else
                    {
                        Console.WriteLine($"{jaartal} is geen schrikkeljaar");
                    }
                    
                }
                else
                {
                    Console.WriteLine($"{jaartal} is een schrikkeljaar");
                }

            
            }
            else
            {
                Console.WriteLine($"{jaartal} is geen schrikkeljaar");
            }



        }

        public static void KeuzeMenu()
        {
            Console.WriteLine("Kies uit volgende methodes door een cijfer te tikken:");
            Console.WriteLine("\n1 - BMIBerekenaar\n2 - Schoenverkoper\n3 - Simpele Rekenmachine\n4 - OhmBerekenaar\n5 - Schrikkeljaar");

            int keuze = Convert.ToInt32(Console.ReadLine());
            Console.Clear();

            switch (keuze)
            {
                case 1:
                    BMIBerekenaar();
                    break;
                case 2:
                    Schoenverkoper();
                    break;
                case 3:
                    SimpeleRekenMachine();
                    break;
                case 4:
                    ohmBerekenaar();
                    break;
                case 5:
                    Schrikkeljaar();
                    break;
            }


        }
    }
}
    
