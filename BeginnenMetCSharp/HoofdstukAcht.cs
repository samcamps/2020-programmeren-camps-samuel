﻿using System;
using System.Linq;
namespace BeginnenMetCSharp
{
    public class HoofdstukAcht
    {
        public static void Opwarmers()
        {
            Console.WriteLine("Maak een array gevuld met de getallen 0 - 10");

            int[] myarray = new int[10];

            for (int i = 0; i < myarray.Length; i++)
            {
                myarray[i] = i;

            }

            for (int i = 0; i < myarray.Length; i++)
            {
                Console.Write($"{myarray[i]} ");

            }

            Console.WriteLine("\n");
            Console.WriteLine("Maak een array gevuld met getallen van 100 tot 1");

            int[] myarray2 = new int[100];
            int j = 0;
            for (int i = myarray2.Length; i > 0; i--)
            {

                myarray2[j] = i;
                j++;

            }
            for (int i = 0; i < myarray2.Length; i++)
            {
                Console.Write($"{myarray2[i]} ");

            }

            
            char[] myarray3 = new char[26];
            char karakter = 'a';
            for (int i =0; i< myarray3.Length;i++)
            {

                myarray3[i] = karakter;
                karakter++;

            }
            for (int i = 0; i < myarray3.Length; i++)
            {
                Console.Write($"{myarray3[i]} ");

            }

            Console.WriteLine("\n");
            Console.WriteLine("Maak een array gevuld met willekeurige getallen tussen 1 en 100 (de array is 20 lang)");


            int[] myArray4 = new int[20];

            Random mygen = new Random();

            for (int i = 0; i < 20; i++)
            {
                myArray4[i] = mygen.Next(1, 100);
                Console.Write($"{myArray4[i]} ");
            }


            Console.WriteLine("\n");
            Console.WriteLine("Maak een array gevuld met afwisseld true en false (de array is 30 lang");

            bool[] myArray5 = new bool[30];


            for (int i = 0; i < myArray5.Length; i++)
            {
                if (i % 2 == 0)
                {
                    myArray5[i] = true;
                }
                else
                {
                    myArray5[i] = false;
                }
                Console.Write($"{myArray5[i]} ");

            }

        }

        public static void ArrayOefener1()
        {
            Console.WriteLine("Geef tien waarden in: ");
            int[] waarden = new int[10];

            for (int i = 0; i < waarden.Length; i++)
            {
                waarden[i] = Convert.ToInt32(Console.ReadLine());
            }

            double som = 0;
            int biggest=0;

            for (int i = 0; i < waarden.Length; i++)
            {
                som = som + waarden[i];
            }

            for (int i = 0; i < waarden.Length; i++)
            {
                if (waarden[i] > biggest)
                {
                    biggest = waarden[i];
                }
            }

            Console.WriteLine($"**********\nSom is {som}, Gemiddelde is {som/waarden.Length:F2}, Grootste getal is {biggest}\n**********");

            int minimum;
            int []result = new int[waarden.Length];
            bool groter = false;
            
            Console.WriteLine("Geef minimum getal in: ");
            minimum = Convert.ToInt32(Console.ReadLine());

            Console.Write($"De getallen groter dan {minimum} zijn: ");
            for (int i = 0; i < waarden.Length; i++)
            {

                if (waarden[i] > minimum && groter == true)
                {
    
                    Console.Write($", {waarden[i]}");
                }

                if (waarden[i] > minimum && groter == false)
                {
                    groter = true;
                    Console.Write($"{waarden[i]}");
                }

            }

            if (groter == false)
            {
                Console.WriteLine("Niets is groter");
            }

        }


        public static void Boodschappenlijstje()
        {
            Console.WriteLine("We gaan de boodschappenlijst samenstellen");
            Console.WriteLine("Hoeveel items wil je opschrijven?");
            int aantal = Convert.ToInt32(Console.ReadLine());

            string[] lijst = new string[aantal];

            for (int i = 0; i < lijst.Length; i++)
            {
                Console.WriteLine($"Wat is item {i+1} op je lijst?");
                lijst[i] = Console.ReadLine();

            }

            Array.Sort(lijst);
            Console.WriteLine("Dit is je gesorteerde lijst: ");
            for (int i = 0; i < lijst.Length; i++)
            {
                Console.WriteLine($"{i + 1}. {lijst[i]}");
            }

            Console.WriteLine("Op naar de winkel!");
            string answer;

            do
            {
                Console.WriteLine("Welk item heb je gekocht?    ");
                string answerItem = Console.ReadLine();
                if (Array.BinarySearch(lijst,answerItem) > 0)
                {
                    lijst[Array.BinarySearch(lijst, answerItem)] = "";

                }
                else
                {
                    Console.WriteLine("Dit item bevindt zich niet op de lijst!");
                }
                Console.WriteLine("Nog winkelen? (Ja of Nee)");
                answer = Console.ReadLine().ToLower();



            } while (answer == "ja");

            Console.WriteLine("Naar huis met de boodschappen!");
            Console.WriteLine("Volgende items van je lijst ben je vergeten te kopen:");

            for (int i = 0; i < lijst.Length; i++)
            {
                Console.Write($"{lijst[i]} ");
            }


        }

        public static void Kerstinkopen()
        {
            double budget;
            Console.WriteLine("Wat is je budget voor je kerstinkopen?");
            budget = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoeveel cadeautjes wil je kopen?");
            int numberGifts = Convert.ToInt32(Console.ReadLine());
            double[] prices = new double [numberGifts];
            double sum = 0;
            for (int i = 0; i < prices.Length; i++)
            {
                Console.WriteLine($"Wat is de prijs van cadeau {i+1}?");
                prices[i] = Convert.ToDouble(Console.ReadLine());
                sum = prices.Sum();
                if (sum > budget)
                {
                    Console.WriteLine($"Je bent al {sum-budget:F1} over het budget!");
                }

            }
            Console.WriteLine();
            Console.WriteLine("Info over je aankopen: ");
            Console.WriteLine($"Totaal bedrag: {sum:F1} euro.\nDuurste cadeau: {prices.Max():F1} euro.\nGoedkoopste cadeau: {prices.Min():F1} euro." +
                $"\nGemiddelde prijs: {prices.Average():F1} euro.");

        }

        public static void ArrayZoeker()
        {
            Random mygen = new Random();
            int[] myarray = new int[10];

            for (int i = 0; i < myarray.Length; i++)
            {
                myarray[i] = mygen.Next(1, 100);
                Console.WriteLine(myarray[i]);
            }

            int remove;
            bool found = false;
            int index = -1;
            Console.WriteLine("Welke waarde moet verwijderd worden?");
            remove = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < myarray.Length; i++)
            {
                if (myarray[i] == remove && found == false)
                {
                    index = i;
                    found = true;
                }


            }

            if (found == false)
            {
                Console.WriteLine("Waarde komt niet voor in de array");
            }

            else
            {
                for (int i = index; i < myarray.Length -1; i++)
                {
                    myarray[i] = myarray[i+1];

                }
                myarray[myarray.Length-1] = -1;

                Console.WriteLine("Resulaat is:");
                for (int i = 0; i < myarray.Length; i++)
                {
                    Console.WriteLine(myarray[i]);
                }

            }
        }



        public static void Koerier()
        {
            Console.WriteLine("Geef gewicht pakket:");
            int gewicht = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef gemeente:");
            int gemeente = Convert.ToInt32(Console.ReadLine());

            int[] gemeenten ={ 1000, 2020, 2013, 4500, 2340, 1200, 9999, 6666, 2362, 2340 };
            int[] prijzen = { 12, 214, 3, 15, 56, 900, 13, 5, 111, 43 };
            bool found = false;
            int index = -1;

            for (int i = 0; i < gemeenten.Length && found == false; i++)
            {
                if (gemeenten[i] == gemeente)
                {
                    index = i;
                    found = true;
                }

            }
            if (found == true)
            {
                Console.WriteLine($"Dit zal {gewicht * prijzen[index]} euro kosten.");
            }

            else
            {
                Console.WriteLine("Wij verzenden niet naar deze gemeente");
            }

        }



        public static void KoerierMulti()
        {
            Console.WriteLine("Geef gewicht pakket:");
            int gewicht = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Geef gemeente:");
            int gemeente = Convert.ToInt32(Console.ReadLine());

            int[,] prijstabel = { { 1000, 12 }, { 2020, 214 }, { 2013, 3 }, { 4500, 15 }, { 2340, 56 }, { 1200, 900 }, { 9999, 13 }, { 6666, 5 }, { 2362, 111 }, { 2340, 43 } };
            bool found = false;
            int[] test = { 100, 200, 300 };
            for (int i = 0; i < prijstabel.GetLength(0) && found == false; i++)
            {
                if (prijstabel[i, 0] == gemeente)
                {
                    Console.WriteLine($"Prijs is {prijstabel[i, 1]*gewicht}");
                    found = true;
                }
               

            }
             
            if (found == false)
            {
                Console.WriteLine("Wij verzenden niet naar deze gemeente");
            }
           


        }

        public static void Hamming()
        {
            Console.WriteLine("Geef twee DNA strings in van gelijke lengte: ");
            string DNA1 = Console.ReadLine();
            string DNA2 = Console.ReadLine();
            string[] indicators = new string[DNA1.Length];

            char[] DNA1char = DNA1.ToCharArray();
            char[] DNA2char = DNA2.ToCharArray();
            int counter = 0;

            for (int i = 0; i < DNA1char.Length; i++)
            {
                if(DNA1char[i] != DNA2char[i])
                {
                    counter++;
                    indicators[i] = "^";

                }
                else
                {
                    indicators[i] = " ";
                }
            }

            for (int i = 0; i < indicators.Length; i++)
            {
                Console.Write(indicators[i]);
            }


            Console.WriteLine($"\nDe hamming distance is {counter}.");

        }

        public static void Galgje()
        {

            string[] woorden = { "computer", "meubel", "dieren", "koffiezet", "fruitschaal" };
            Random mygen = new Random();
            string toFind = woorden[mygen.Next(0, 5)];
            char[] toFindchar = toFind.ToCharArray();
            char[] toShow = new char[toFindchar.Length];
            bool found = false;
            string answer;
            int foundcount;
            int pogingen=0;
            
            for (int i = 0; i <toFindchar.Length; i++)
            {
                toShow[i] = '*';
     
            }

            while(found == false)
            {
                pogingen++;

                for (int i = 0; i < toFindchar.Length; i++)
                {
                    Console.Write(toShow[i]);
                }  

                Console.WriteLine("\nGeef letter in of typ het volledige woord indien je denkt het te weten:");
                answer = Console.ReadLine();
                char[] answerchar = answer.ToCharArray();
                if (answer.Length == 1)
                {
                    for (int i = 0; i < toFindchar.Length; i++)
                    {
                        if (answer[0] == toFindchar[i])
                        {
                            toShow[i] = answer[0];
                        }
                    }
                }
                else if (answer.Length == toFind.Length)
                {
                    foundcount = 0;
                    for (int i = 0; i < toFindchar.Length; i++)
                    {
                        if (answer[i] == toFindchar[i])
                        {
                            foundcount++;
                        }
                    }
                    if (foundcount == toFind.Length)
                    {
                        found = true;
                    }
                    else
                    {
                        Console.WriteLine("Niet correct! Probeer een letter in te geven");
                    }
                    

                }
                else
                {
                    Console.WriteLine("Geef een letter in of geef een antwoord met de correcte lengte");
                }
                Console.WriteLine();

            }

            Console.WriteLine("Correct! U hebt het juiste antwoord gevonden");
            Console.WriteLine($"Aantal pogingen: {pogingen}.");


        }

      

    }
}
