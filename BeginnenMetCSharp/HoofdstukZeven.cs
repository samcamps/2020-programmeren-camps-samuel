﻿using System;
namespace BeginnenMetCSharp
{
    public class HoofdstukZeven
    {
        /// <summary>
        /// Kwadrateert het getal 
        /// </summary>
        /// <param name="getal">int getal</param>
        /// <returns>int kwadraat</returns>

        public static int Square(int getal)
        {
            int result = getal * getal;
            return result;

        }

        /// <summary>
        /// Maakt van den diameter de straal
        /// </summary>
        /// <param name="diameter">double diameter</param>
        /// <returns>double diameter /2</returns>
        public static double Radius(double diameter)
        {
            double result = diameter / 2;
            return result;

        }
        /// <summary>
        /// Berekent de omtrek
        /// </summary>
        /// <param name="straal">double straal</param>
        /// <returns>double omtrek</returns>
        public static double Circumfrence(double straal)
        {
            double result = 2 * Math.PI * straal;
            return result;

        }

        /// <summary>
        /// Berekent de oppervlakte
        /// </summary>
        /// <param name="straal">double straal</param>
        /// <returns><double F2 oppervlakte</returns>

        public static double Surface(double straal)
        {
            double result = Math.PI * straal * straal;
            return result;
        }
        /// <summary>
        /// Geeft het grootste getal weer van 2
        /// </summary>
        /// <param name="a"> int getal</param>
        /// <param name="b">int getal</param>
        /// <returns>int grootste </returns>
        public static int Largest(int a, int b)
        {
            if (a > b)
            {
                return a;
            }
            else
            {
                return b;
            }

        }
        /// <summary>
        /// Geeft weer of input even is 
        /// </summary>
        /// <param name="a">int a</param>
        /// <returns>bool true indien even, false als oneven</returns>

        public static bool IsEven(int a)
        {
            if (a % 2 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Toont de oneven nummers op de console tot aan de input
        /// </summary>
        /// <param name="n">int input</param>

        public static void ShowOdd(int n)
        {
            for (int i = 1; i < n; i++)
            {
                if (i % 2 == 1)
                {
                    Console.WriteLine(i);
                }
            }
        }

        /// <summary>
        /// Roept een reeks methoden op
        /// </summary>

        public static void DemonstreerKleintjes()
        {
            int answer;
            Console.WriteLine("Wat moet ik kwadrateren?");
            answer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Square(answer));
            Console.WriteLine("Wat is de diameter?");
            answer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Radius(answer));

            Console.WriteLine("Wat is de diameter?");
            answer = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"De omtrek is {Circumfrence(answer):F2}");
            Console.WriteLine($"De oppervlakte is {Surface(answer):F2}");
            Console.WriteLine("Welke twee getallen wil je vergelijken?");
            int getal1 = Convert.ToInt32(Console.ReadLine());
            int getal2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"{Largest(getal1, getal2)} is het grootste getal");
            Console.WriteLine("Geef een getal en ik zeg of het even is:");
            answer = Convert.ToInt32(Console.ReadLine());
            if (IsEven(answer))
            {
                Console.WriteLine("Het getal is even");
            }
            else
            {
                Console.WriteLine("Het getal is niet even");
            }
            Console.WriteLine("Geef een getal en ik geef de oneven getallen");
            answer = Convert.ToInt32(Console.ReadLine());
            ShowOdd(answer);

        }
        /// <summary>
        ///vergelijkt 3 getallen en
        ///geeft grootste terug
        /// </summary>
        /// <param name="a">int</param>
        /// <param name="b">int</param>
        /// <param name="c">int</param>
        /// <returns>int grotste getal</returns>

        public static int Grootste(int a, int b, int c)
        {
            if (a > b && a > c)
            {
                return a;
            }
            else if (b > a && b > c)
            {
                return b;
            }
            else
            {
                return c;
            }

        }

        /// <summary>
        /// vraagt drie int's
        /// roept methode Grootste op
        /// </summary>

        public static void VraagDrieEnToonGrootste()
        {
            int a; int b; int c;
            Console.WriteLine("Geef drie getallen:");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            c = Convert.ToInt32(Console.ReadLine());

            int result = Grootste(a, b, c);
            Console.WriteLine($"Grootste getal is {result}");
        }


        /// <summary>
        /// Geeft de titel duur en categorie 
        /// </summary>
        /// <param name="name">Naam van de film</param>
        /// <param name="duur">Duur in int</param>
        /// <param name="genre">Enum type</param>
        /// <returns></returns>

        public enum FilmGenre
        {

            Drama, Action, Comedy, Uncategorized
        }

        public static void FilmRuntime(string name, int duur = 90, FilmGenre genre = FilmGenre.Uncategorized)
        {
            Console.WriteLine($"{name} ({duur} minuten, {genre})");
        }


        public static void DemonstreerFilm()
        {
            FilmRuntime("The Matrix", 120, FilmGenre.Action);
            FilmRuntime("Crouching Tiger, Hddden Dragon", duur: 90, genre: FilmGenre.Uncategorized);
            FilmRuntime(duur: 50, name: "Die Hard");
        }



        public static double TelOp(double a, double b)
        {
            return a + b;
        }

        public static double TrekAf(double a, double b)
        {
            return a - b;
        }

        public static double Vermenigvuldig(double a, double b)
        {
            return a * b;
        }

        public static double Deel(double a, double b)
        {
            return a / b;
        }

        enum Keuzes
        {
            NA, Optellen, Aftrekken, Vermenigvuldigen, Delen
        }

        public static void DemonstreerOperaties()
        {
            Console.WriteLine("Maak een keuze uit: ");
            Console.WriteLine($"{(int)Keuzes.Optellen}. {Keuzes.Optellen}");
            Console.WriteLine($"{(int)Keuzes.Aftrekken}. {Keuzes.Aftrekken}");
            Console.WriteLine($"{(int)Keuzes.Vermenigvuldigen}. {Keuzes.Vermenigvuldigen}");
            Console.WriteLine($"{(int)Keuzes.Delen}. {Keuzes.Delen}");

            Console.WriteLine("Geef het overeenstemmende nummer als keuze in:");
            int keuze = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Geef het eerste nummer in:");
            double a = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Geef het tweede nummer in:");
            double b = Convert.ToDouble(Console.ReadLine());
            double resultaat = 0;
            string symbool = "";

            switch (keuze) {
                case 1:
                    resultaat = TelOp(a, b);
                    symbool = "+";
                    break;

                case 2:
                    resultaat = TrekAf(a, b);
                    symbool = "-";
                    break;

                case 3:
                    resultaat = Vermenigvuldig(a, b);
                    symbool = "*";
                    break;

                case 4:
                    resultaat = Deel(a, b);
                    symbool = "/";
                    break;

            }
            Console.WriteLine($"Resultaat: ({a} {symbool} {b}) = {resultaat}");
        }

        /// <summary>
        /// vraagt naam en zegt hallo
        /// </summary>

        public static void Hallo()
        {
            Console.WriteLine("Methode is opgeroepen zonder argument");
            Console.WriteLine("Wat is je naam?");
            string name = Console.ReadLine();
            Console.WriteLine($"Hallo, {name}!");
        }

        /// <summary>
        /// neemt 1 parameter en zegt hallo
        /// </summary>
        /// <param name="name">string naam</param>

        public static void Hallo(string name)
        {
            Console.WriteLine("Methode is opgeroepen met 1 argument");
            Console.WriteLine($"Hallo, {name}!");
        }
        /// <summary>
        /// neemt 2 namen en zegt hallo
        /// </summary>
        /// <param name="name">string eerste naam</param>
        /// <param name="name2">string 2e naam</param>

        public static void Hallo(string name, string name2)
        {
            Console.WriteLine("Methode is opgeroepen met 2 argumenten");
            Console.WriteLine($"Hallo, {name} en {name2}!");
        }
        /// <summary>
        /// vraagt 3 namen en roept dan methode hallo op zonder argument
        /// met 1 en 2 argumenten
        /// </summary>

        public static void DemonstreerHallo()
        {
            Console.WriteLine("DemonstreerHalo heeft drie namen nodig!");
            string name1 = Console.ReadLine();
            string name2 = Console.ReadLine();
            string name3 = Console.ReadLine();

            Hallo();
            Hallo(name1);
            Hallo(name2, name3);

        }
    }




}

