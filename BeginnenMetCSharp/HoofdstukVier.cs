﻿using System;
namespace BeginnenMetCSharp
{
    public class HoofdstukVier
    {
        public static void BMIBerekenaar() 
        {
            Console.WriteLine("Hoeveel weeg je in kg?");
            double weight = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Hoe groot ben je in m?");
            double length = Convert.ToDouble(Console.ReadLine());
            double result = weight / Math.Pow(length, 2);
            Console.WriteLine($"Je BMI bedraagt {result:F2}.");
        }

        public static void Pythagoras()
        {
            Console.WriteLine("Geef de lengte van de eerste rechthoekszijde:");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Geef de lengte van de tweede rechthoekszijde:");
            double b = Convert.ToDouble(Console.ReadLine());
            double c = Math.Pow((Math.Pow(a, 2) + Math.Pow(b, 2)), 0.5);
            Console.WriteLine($"De lengte van de schuine zijde is {c:F2}");
        }


        public static void Cirkels()
        {
            Console.WriteLine("Geef de straal:");
            double radius = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"De omtrek van een cirkel met straal {radius} is {2 * radius * Math.PI:F2}");
            Console.WriteLine($"De oppervlakte is {Math.PI * Math.Pow(radius, 2):F2}");

        }

        public static void Orakeltje()
        {
            Console.Write("Hoe oud ben je nu? ");
            int leeftijd = Convert.ToInt32(Console.ReadLine());
            Random mygen = new Random();
            int getal = mygen.Next(20, 120);
            Console.WriteLine($"Je zal nog {getal} jaar leven. Je zal dus {leeftijd + getal} worden.");

        }

        public static void KeuzeMenu()
        {
            Console.WriteLine("Kies uit volgende methodes door een cijfer te tikken:");
            Console.WriteLine("1 - BMIBerekenaar\n2 - Pythagoras\n3 - Cirkels\n4 - Orakeltje");

            int keuze = Convert.ToInt32(Console.ReadLine());

            switch (keuze)
            {
                case 1:
                    BMIBerekenaar();
                    break;
                case 2:
                    Pythagoras();
                    break;
                case 3:
                    Cirkels();
                    break;
                case 4:
                    Orakeltje();
                    break;
            }

        }

    }
}
