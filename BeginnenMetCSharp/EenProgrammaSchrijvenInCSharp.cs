﻿using System;
namespace BeginnenMetCSharp
{
    public class EenProgrammaSchrijvenInCSharp
    {
        public static void ZegGoeienDag()
        {
            Console.Write("Hoe heet je? ");
            string invoer = Console.ReadLine();
            Console.Write(invoer);
            Console.WriteLine(", ik wens je een fijne dag");



        }


        public static void MijnEersteProgramma()
        {
            Console.WriteLine("Dit is mijn eerste C#-programma");
            Console.WriteLine("*******************************");
            Console.Write("Typ je voornaam: ");
            string firstName = Console.ReadLine();
            Console.Write("Typ je achternaam: ");
            string lastName = Console.ReadLine();
            Console.WriteLine();
            Console.WriteLine("Dus je naam is " + lastName + " " + firstName);
            Console.WriteLine("Of: " + firstName + " " + lastName);




        }


        static public void RommelZin()
        {
            Console.WriteLine("Wat is je favoriete kleur? ");
            string color = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete eten? ");
            string food = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete auto? ");
            string car = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete film? ");
            string movie = Console.ReadLine();
            Console.WriteLine("Wat is je favoriete boek? ");
            string book = Console.ReadLine();

            Console.Write("Je favoriete kleur is " + food + ". Je eet graag " + car + ". Je lievingsfilm is " + book + " en je favoriete boek is " + movie +".");


        }



        static public void GekleurdeRommelZin()
        {
           
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Wat is je favoriete kleur? ");

            Console.ForegroundColor = ConsoleColor.White;
            string color = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Wat is je favoriete eten? ");

            Console.ForegroundColor = ConsoleColor.White;
            string food = Console.ReadLine();


            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("Wat is je favoriete auto? ");

            Console.ForegroundColor = ConsoleColor.White;
            string car = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Wat is je favoriete film? ");
            
            Console.ForegroundColor = ConsoleColor.White;
            string movie = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("Wat is je favoriete boek? ");

            Console.ForegroundColor = ConsoleColor.White;
            string book = Console.ReadLine();

            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Je favoriete kleur is " + food + ". Je eet graag " + car + ". Je lievingsfilm is " + book + " en je favoriete boek is " + movie + ".");


        }
    }
}
